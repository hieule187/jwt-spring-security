package com.spring.jwt_spring_security.repository.mysql;

import com.spring.jwt_spring_security.entity.Role;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByName(String name);

  Boolean existsByName(String name);

  @Query(
      value = "select r.* "
          + "from user u "
          + "join user_role ur on u.id = ur.user_id "
          + "join role r on r.id = ur.role_id "
          + "where u.username = :username", nativeQuery = true
  )
  List<Role> getRole(@Param("username") String username);

//  List<Role> findByUsersUsername(String username);
}
