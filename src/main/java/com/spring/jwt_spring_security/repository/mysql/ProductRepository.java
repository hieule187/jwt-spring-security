package com.spring.jwt_spring_security.repository.mysql;

import com.spring.jwt_spring_security.entity.Product;
import com.spring.jwt_spring_security.config.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends BaseRepository<Product> {

}
