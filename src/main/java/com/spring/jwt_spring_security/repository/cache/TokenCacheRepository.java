package com.spring.jwt_spring_security.repository.cache;

import com.spring.jwt_spring_security.model.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenCacheRepository extends CrudRepository<Token, String> {

}
