package com.spring.jwt_spring_security.model;

import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Data
@RedisHash("TOKEN_CACHE")
public class Token implements Serializable {

  @Id
  private Long userId;

  private String username;

}
