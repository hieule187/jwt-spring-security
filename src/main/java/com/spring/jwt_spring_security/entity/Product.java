package com.spring.jwt_spring_security.entity;

import com.spring.jwt_spring_security.config.base.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Product extends BaseEntity {

  private String productName;

  private String productPrice;

  private String productQuantity;

  public static Product of(String productName, String productPrice, String productQuantity) {
    Product product = new Product();
    product.setProductName(productName);
    product.setProductPrice(productPrice);
    product.setProductQuantity(productQuantity);

    return product;
  }

}
