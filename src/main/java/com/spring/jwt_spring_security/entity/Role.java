package com.spring.jwt_spring_security.entity;

import com.spring.jwt_spring_security.config.base.BaseEntity;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Role extends BaseEntity {

  @Column(unique = true)
  private String name;

  private String description;

//  @ManyToMany(mappedBy = "roles")
//  private Set<User> users = new HashSet<>();

  public static Role of(String name, String description) {
    Role role = new Role();
    role.setName(name);
    role.setDescription(description);

    return role;
  }

}
