package com.spring.jwt_spring_security.config.exception;

import com.spring.jwt_spring_security.constant.KeyMappingConstant;
import com.spring.jwt_spring_security.dto.response.ErrorResponse;
import io.jsonwebtoken.JwtException;
import java.sql.BatchUpdateException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.HeuristicCompletionException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  @Value("${application.i18n.locale.language:en}")
  String language;

  private final MessageSource messageSource;

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleAll(
      Exception exception, HttpServletRequest request
  ) {
    log.error(
        "(handleAll)user: {}, path: {}, status: {}, exception: {}",
        request.getUserPrincipal(), request.getRequestURI(),
        HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ExceptionUtils.getStackTrace(exception)
    );

    String error = "com.spring.jwt_spring_security.exception.InternalServerError";
    Map<String, String> params = new HashMap<>();
    params.put(KeyMappingConstant.DETAIL, exception.getMessage());

    return getResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error, params);
  }

  @ExceptionHandler(CustomRuntimeException.class)
  protected ResponseEntity<Object> handleCustomRuntime(
      CustomRuntimeException exception, HttpServletRequest request
  ) {
    log.error(
        "(handleCustomRuntime)user: {}, path: {}, status: {}, error: {}, params: {}",
        request.getUserPrincipal(), request.getRequestURI(), exception.getStatus(),
        exception.getError(), exception.getParams()
    );

    return getResponse(exception.getStatus(), exception.getError(), exception.getParams());
  }

  @ExceptionHandler({
      SQLIntegrityConstraintViolationException.class,
      BatchUpdateException.class,
      ConstraintViolationException.class,
      DataIntegrityViolationException.class,
      HeuristicCompletionException.class
  })
  public ResponseEntity<Object> handleConstraintViolation(
      Exception exception, HttpServletRequest request
  ) {
    log.error(
        "(handleConstraintViolation)user: {}, path: {}, status: {}, message: {}",
        request.getUserPrincipal(), request.getRequestURI(),
        HttpServletResponse.SC_BAD_REQUEST, exception.getMessage()
    );

    String error = "com.spring.jwt_spring_security.exception.ConstraintViolationException";
    Map<String, String> params = new HashMap<>();
    params.put(KeyMappingConstant.DETAIL, exception.getMessage());

    return getResponse(HttpServletResponse.SC_BAD_REQUEST, error, params);
  }

  @ExceptionHandler(JwtException.class)
  public ResponseEntity<Object> handleInvalidToken(
      JwtException exception, HttpServletRequest request
  ) {
    log.error(
        "(handleInvalidToken)user: {}, path: {}, status: {}, message: {}",
        request.getUserPrincipal(), request.getRequestURI(),
        HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage()
    );

    String error = "com.spring.jwt_spring_security.exception.InvalidTokenException";
    Map<String, String> params = new HashMap<>();

    return getResponse(HttpServletResponse.SC_UNAUTHORIZED, error, params);
  }

  @ExceptionHandler(AccessDeniedException.class)
  protected ResponseEntity<Object> handleAccessDenied(
      AccessDeniedException exception, HttpServletRequest request
  ) {
    log.error(
        "(handleAccessDenied)user: {}, path: {}, status: {}, message: {}",
        request.getUserPrincipal(), request.getRequestURI(),
        HttpServletResponse.SC_FORBIDDEN, exception.getMessage()
    );

    String error = "com.spring.jwt_spring_security.exception.AccessDeniedException";
    Map<String, String> params = new HashMap<>();

    return getResponse(HttpServletResponse.SC_FORBIDDEN, error, params);
  }

  @Override
  protected @NonNull ResponseEntity<Object> handleMissingServletRequestParameter(
      @NonNull MissingServletRequestParameterException exception,
      @NonNull HttpHeaders headers,
      @NonNull HttpStatus status,
      @NonNull WebRequest request
  ) {
    String error = "com.spring.jwt_spring_security.exception.MissingParameterException";
    Map<String, String> params = new HashMap<>();
    params.put(KeyMappingConstant.PARAMETER_NAME, exception.getParameterName());
    params.put(KeyMappingConstant.PARAMETER_TYPE, exception.getParameterType());

    return getResponse(status.value(), error, params);
  }

  @Override
  protected @NonNull ResponseEntity<Object> handleTypeMismatch(
      @NonNull TypeMismatchException exception,
      @NonNull HttpHeaders headers,
      @NonNull HttpStatus status,
      @NonNull WebRequest request
  ) {
    String error = "com.spring.jwt_spring_security.exception.TypeMismatchException";
    Map<String, String> params = new HashMap<>();
    params.put(KeyMappingConstant.DETAIL, exception.getMessage());

    return getResponse(status.value(), error, params);
  }

  @Override
  protected @NonNull ResponseEntity<Object> handleMethodArgumentNotValid(
      @NonNull MethodArgumentNotValidException exception,
      @NonNull HttpHeaders headers,
      @NonNull HttpStatus status,
      @NonNull WebRequest request
  ) {
    String error = "com.spring.jwt_spring_security.exception.MethodArgumentNotValidException";
    Map<String, String> params = new HashMap<>();

    FieldError fieldError = exception.getBindingResult().getFieldError();

    if (fieldError != null) {
      params.put(KeyMappingConstant.FIELD, fieldError.getField());
      params.put(KeyMappingConstant.OBJECT_NAME, fieldError.getObjectName());
      params.put(KeyMappingConstant.DEFAULT_MESSAGE, fieldError.getDefaultMessage());
    }

    return getResponse(status.value(), error, params);
  }

  @Override
  protected @NonNull ResponseEntity<Object> handleHttpMessageNotReadable(
      @NonNull HttpMessageNotReadableException exception,
      @NonNull HttpHeaders headers,
      @NonNull HttpStatus status,
      @NonNull WebRequest request
  ) {
    String error = "com.spring.jwt_spring_security.exception.HttpMessageNotReadableException";
    Map<String, String> params = new HashMap<>();
    params.put(KeyMappingConstant.DETAIL, exception.getMessage());

    return getResponse(status.value(), error, params);
  }

  @Override
  protected @NonNull ResponseEntity<Object> handleHttpRequestMethodNotSupported(
      @NonNull HttpRequestMethodNotSupportedException exception,
      @NonNull HttpHeaders headers,
      @NonNull HttpStatus status,
      @NonNull WebRequest request
  ) {
    String error = "com.spring.jwt_spring_security.exception.MethodNotSupportedException";
    Map<String, String> params = new HashMap<>();

    return getResponse(status.value(), error, params);
  }

  @Override
  protected @NonNull ResponseEntity<Object> handleNoHandlerFoundException(
      @NonNull NoHandlerFoundException exception,
      @NonNull HttpHeaders headers,
      @NonNull HttpStatus status,
      @NonNull WebRequest request
  ) {
    String error = "com.spring.jwt_spring_security.exception.NoHandlerFoundException";
    Map<String, String> params = new HashMap<>();
    params.put(KeyMappingConstant.PATH, exception.getRequestURL());

    return getResponse(status.value(), error, params);
  }

  private ResponseEntity<Object> getResponse(
      Integer status, String error, Map<String, String> params
  ) {
    log.debug("(getResponse)status: {}, error: {}, params: {}", status, error, params);

    String message = getMessage(error, params);

    return new ResponseEntity<>(
        ErrorResponse.of(status, error, message),
        HttpStatus.valueOf(status)
    );
  }

  private String getMessage(String error, Map<String, String> params) {
    log.debug("(getMessage)error: {}, params: {}", error, params);

    String message = messageSource.getMessage(error, null, new Locale(language));
    log.debug("(getMessage)message: {}", message);

    if (!params.isEmpty()) {
      for (Map.Entry<String, String> param : params.entrySet()) {
        String key = param.getKey();
        String value = param.getValue();
        if (key != null && value != null) {
          message = message.replace("%" + key + "%", value);
        }
      }
      log.debug("(getMessage)message replaced: {}", message);
    }

    return message;
  }

}
