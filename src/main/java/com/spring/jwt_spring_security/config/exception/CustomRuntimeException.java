package com.spring.jwt_spring_security.config.exception;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;

@Data
public class CustomRuntimeException extends RuntimeException {

  private Integer status = 0;

  private String error = "";

  private Map<String, String> params = new HashMap<>();

  public void addParam(String key, String value) {
    if (params == null) {
      params = new HashMap<>();
    }
    params.put(key, value);
  }

}
