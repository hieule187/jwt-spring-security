package com.spring.jwt_spring_security.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class TopicConfig {

  @Value("${application.kafka.topic.create_product}")
  private String topicCreateProduct;

  @Value("${application.kafka.topic.partition-count:3}")
  private int topicPartitionCount;

  @Bean
  public NewTopic topicCreateProduct() {
    return TopicBuilder.name(topicCreateProduct)
        .partitions(topicPartitionCount)
        .build();
  }

}
