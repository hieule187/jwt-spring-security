package com.spring.jwt_spring_security.config.rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {

  @Value("${application.rabbitmq.queue.create_product}")
  private String queueCreateProduct;

  @Bean
  public Queue queueCreateProduct() {
    return new Queue(queueCreateProduct);
  }

}
