package com.spring.jwt_spring_security.config.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.jwt_spring_security.dto.response.ErrorResponse;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, securedEnabled = true)
public class SecurityConfig {

  @Value("${application.i18n.locale.language:en}")
  String language;

  private final AuthenticationFilter authenticationFilter;
  private final AuthenticationProvider authenticationProvider;
  private final MessageSource messageSource;

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    http
        .csrf()
        .disable()
        .authorizeHttpRequests()
        .antMatchers(AUTH_WHITELIST)
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authenticationProvider(authenticationProvider)
        .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class)
        .exceptionHandling()
        .authenticationEntryPoint(authenticationEntryPoint());

    return http.build();
  }

  private static final String[] AUTH_WHITELIST = {
      "/v3/api-docs/**",
      "/v3/api-docs.yaml",
      "/swagger-ui/**",
      "/swagger-ui.html",
      "/api/v1/auth/**",
      "/api/v1/**/public/**"
  };

  private AuthenticationEntryPoint authenticationEntryPoint() {
    return (request, response, exception) -> {
      String error = "com.spring.jwt_spring_security.exception.UnauthorizedException";
      String message = messageSource.getMessage(error, null, new Locale(language));
      ErrorResponse errorResponse = ErrorResponse.of(
          HttpServletResponse.SC_UNAUTHORIZED, error, message
      );
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      new ObjectMapper().writeValue(response.getOutputStream(), errorResponse);
      response.getOutputStream().flush();
    };
  }

}
