package com.spring.jwt_spring_security.config.base;

import java.util.List;
import org.springframework.data.domain.Page;

public interface BaseService<T extends BaseEntity> {

  T findById(String id);

  T findByIdThrowNotFound(String id);

  List<T> findByIds(List<String> ids);

  List<T> findAll();

  List<T> findAllList(int page, int size);

  Page<T> findAllPage(int page, int size);

  T create(T t);

  List<T> createAll(List<T> listOfT);

  T update(T t);

  List<T> updateAll(List<T> listOfT);

  void deleteById(String id);

  void deleteByIds(List<String> ids);

}
