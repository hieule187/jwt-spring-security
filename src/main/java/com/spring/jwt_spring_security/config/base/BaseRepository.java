package com.spring.jwt_spring_security.config.base;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRepository<T> extends JpaRepository<T, String> {

}
