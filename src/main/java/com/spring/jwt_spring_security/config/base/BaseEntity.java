package com.spring.jwt_spring_security.config.base;

import java.util.UUID;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Data
@MappedSuperclass
public class BaseEntity {

  @Id
  @org.springframework.data.annotation.Id
  private String id;

  @CreatedDate
  private Long createdAt;

  @LastModifiedDate
  private Long updatedAt;

  @CreatedBy
  private String createdBy;

  @LastModifiedBy
  private String updatedBy;

  @PrePersist
  private void ensureId() {
    if (this.getId() == null || this.getId().isEmpty()) {
      this.setId(UUID.randomUUID().toString());
    }
  }

}
