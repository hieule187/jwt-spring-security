package com.spring.jwt_spring_security.config.base;

import com.spring.jwt_spring_security.exception.NotFoundException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@Slf4j
public class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

  private final BaseRepository<T> repository;

  public BaseServiceImpl(BaseRepository<T> repository) {
    this.repository = repository;
  }

  @Override
  public T findById(String id) {
    log.info("(findById)id: {}", id);
    return repository.findById(id).orElse(null);
  }

  @Override
  public T findByIdThrowNotFound(String id) {
    log.info("(findByIdThrowNotFound)id: {}", id);
    return repository.findById(id).orElseThrow(() -> new NotFoundException(id));
  }

  @Override
  public List<T> findByIds(List<String> ids) {
    log.info("(findByIds)ids: {}", ids);
    return repository.findAllById(ids);
  }

  @Override
  public List<T> findAll() {
    return repository.findAll();
  }

  @Override
  public List<T> findAllList(int page, int size) {
    return repository.findAll(PageRequest.of(page, size)).getContent();
  }

  @Override
  public Page<T> findAllPage(int page, int size) {
    return repository.findAll(PageRequest.of(page, size));
  }

  @Override
  public T create(T t) {
    log.info("(create)object: {}", t);
    return repository.save(t);
  }

  @Override
  public List<T> createAll(List<T> listOfT) {
    log.info("(createAll)objects: {}", listOfT);
    return repository.saveAll(listOfT);
  }

  @Override
  public T update(T t) {
    log.info("(update)object: {}", t);
    return repository.save(t);
  }

  @Override
  public List<T> updateAll(List<T> listOfT) {
    log.info("(updateAll)objects: {}", listOfT);
    return repository.saveAll(listOfT);
  }

  @Override
  public void deleteById(String id) {
    log.info("(deleteById)id: {}", id);
    repository.deleteById(id);
  }

  @Override
  public void deleteByIds(List<String> ids) {
    log.info("(deleteByIds)ids: {}", ids);
    repository.deleteAllById(ids);
  }

}
