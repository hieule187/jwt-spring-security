package com.spring.jwt_spring_security.config.audit;

import com.spring.jwt_spring_security.constant.CommonConstant;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
public class AuditorAwareImpl implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    String currentUser = CommonConstant.DEFAULT_CURRENT_USER;

    if (SecurityContextHolder.getContext().getAuthentication() != null) {
      // userId
      currentUser = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
    }
    log.debug("(getCurrentAuditor)currentUser: {}", currentUser);

    return Optional.of(currentUser);
  }

}
