package com.spring.jwt_spring_security.config.i18n;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.i18n")
@Data
public class MessageResourceProperties {

  private List<String> resources;

}

