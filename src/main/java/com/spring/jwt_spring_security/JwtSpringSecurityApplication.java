package com.spring.jwt_spring_security;

import com.spring.jwt_spring_security.config.audit.AuditorAwareImpl;
import com.spring.jwt_spring_security.entity.Role;
import com.spring.jwt_spring_security.entity.User;
import com.spring.jwt_spring_security.service.UserService;
import java.util.HashSet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
@EntityScan(basePackages = {"com.spring.jwt_spring_security.entity"})
@EnableJpaRepositories(basePackages = {"com.spring.jwt_spring_security.repository.mysql"})
@EnableJpaAuditing
@Slf4j
public class JwtSpringSecurityApplication {

  @Bean
  public AuditorAware<String> auditorAware() {
    return new AuditorAwareImpl();
  }

  public static void main(String[] args) {
    SpringApplication.run(JwtSpringSecurityApplication.class, args);
  }

  @Bean
  CommandLineRunner run(UserService userService) {
    return args -> {
      userService.saveRole(Role.of("ROLE_ADMIN", "Quản trị viên"));
      userService.saveRole(Role.of("ROLE_MANAGER", "Quản lý"));
      userService.saveRole(Role.of("ROLE_USER", "Người dùng"));
      userService.saveRole(Role.of("ROLE_USER_PREMIUM", "Người dùng cao cấp"));

      userService.saveUser(new User
          ("admin@gmail.com", "admin", "hieult", "0123456789", new HashSet<>())
      );
      userService.saveUser(new User
          ("hieult@gmail.com", "hieult", "hieult", "0123456789", new HashSet<>())
      );

      userService.addRoleToUser("admin", "ROLE_ADMIN");
      userService.addRoleToUser("admin", "ROLE_MANAGER");
      userService.addRoleToUser("hieult", "ROLE_MANAGER");
      userService.addRoleToUser("hieult", "ROLE_USER");
      userService.addRoleToUser("hieult", "ROLE_USER_PREMIUM");

//      userService.getRoleByUsername("hieult1");
    };
  }

}
