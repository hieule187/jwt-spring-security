package com.spring.jwt_spring_security.exception;

import com.spring.jwt_spring_security.config.exception.CustomRuntimeException;
import java.util.Map;

public class CustomException extends CustomRuntimeException {

  public CustomException(Integer status, String error) {
    setStatus(status);
    setError(error);
  }

  public CustomException(Integer status, String error, Map<String, String> params) {
    setStatus(status);
    setError(error);
    setParams(params);
  }

}
