package com.spring.jwt_spring_security.exception;

import com.spring.jwt_spring_security.config.exception.CustomRuntimeException;
import javax.servlet.http.HttpServletResponse;

public class NotFoundException extends CustomRuntimeException {

  public NotFoundException(String id) {
    setStatus(HttpServletResponse.SC_NOT_FOUND);
    setError("com.spring.jwt_spring_security.exception.NotFoundException");
    addParam("id", id);
  }

}
