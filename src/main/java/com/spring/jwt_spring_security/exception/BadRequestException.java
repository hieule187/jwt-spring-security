package com.spring.jwt_spring_security.exception;

import com.spring.jwt_spring_security.config.exception.CustomRuntimeException;
import javax.servlet.http.HttpServletResponse;

public class BadRequestException extends CustomRuntimeException {

  public BadRequestException(String error) {
    setStatus(HttpServletResponse.SC_BAD_REQUEST);
    setError(error);
  }

}
