package com.spring.jwt_spring_security.constant;

public class RoleConstant {

  private RoleConstant() {
  }

  public static final String ADMIN = "ROLE_ADMIN";
  public static final String MANAGER = "ROLE_MANAGER";
  public static final String USER = "ROLE_USER";
  public static final String USER_PREMIUM = "ROLE_USER_PREMIUM";

}
