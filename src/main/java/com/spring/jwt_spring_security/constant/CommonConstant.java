package com.spring.jwt_spring_security.constant;

public class CommonConstant {

  private CommonConstant() {
  }

  public static final String DEFAULT_CURRENT_USER = "SYSTEM";

}
