package com.spring.jwt_spring_security.constant;

public class KeyMappingConstant {

  private KeyMappingConstant() {
  }

  public static final String DETAIL = "detail";
  public static final String PARAMETER_NAME = "parameterName";
  public static final String PARAMETER_TYPE = "parameterType";
  public static final String FIELD = "field";
  public static final String OBJECT_NAME = "objectName";
  public static final String DEFAULT_MESSAGE = "defaultMessage";
  public static final String PATH = "path";

}
