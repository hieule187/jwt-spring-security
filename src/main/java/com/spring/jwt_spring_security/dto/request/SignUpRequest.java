package com.spring.jwt_spring_security.dto.request;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SignUpRequest {

  private String name;

  private String email;

  private String username;

  private String password;

  private String phoneNumber;

  private List<String> roles;
}
