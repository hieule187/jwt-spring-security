package com.spring.jwt_spring_security.dto;

import lombok.Data;

@Data
public class ProductDTO {

  private String id;

  private String productName;

  private String productPrice;

  private String productQuantity;

}
