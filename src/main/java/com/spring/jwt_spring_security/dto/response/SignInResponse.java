package com.spring.jwt_spring_security.dto.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SignInResponse {

  private String accessToken;

  private String refreshToken;

  private Long userId;

  private String username;

  private List<String> roles;
}
