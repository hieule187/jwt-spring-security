package com.spring.jwt_spring_security.service;

import java.util.List;

public interface RabbitmqService {

  void push(String queue, String message);

  void push(String queue, Object message);

  void push(String queue, List<Object> messages);

}
