package com.spring.jwt_spring_security.service;

import com.spring.jwt_spring_security.model.Token;
import java.util.Map;

public interface RedisService {

  void saveToken(Token token);

  Map<Object, Token> findAllToken();

  Token findTokenByUserId(Long userId);

  void updateToken(Token token);

  void deleteToken(Long userId);

}
