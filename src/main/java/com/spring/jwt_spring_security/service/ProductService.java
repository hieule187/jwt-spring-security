package com.spring.jwt_spring_security.service;

import com.spring.jwt_spring_security.config.base.BaseService;
import com.spring.jwt_spring_security.entity.Product;

public interface ProductService extends BaseService<Product> {

  void rabbitmqCreate(Product product);

  void kafkaCreate(Product product);

}
