package com.spring.jwt_spring_security.service;

import com.spring.jwt_spring_security.entity.Role;
import com.spring.jwt_spring_security.entity.User;
import java.util.Optional;

public interface UserService {
  Optional<User> findByUsername(String username);

  void saveUser(User user);

  void saveRole(Role role);

  void addRoleToUser(String username, String roleName);

  void getRoleByUsername(String username);

  String test(int pram1);

}
