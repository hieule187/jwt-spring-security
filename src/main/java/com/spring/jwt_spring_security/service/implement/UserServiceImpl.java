package com.spring.jwt_spring_security.service.implement;

import com.spring.jwt_spring_security.dto.response.RoleResponse;
import com.spring.jwt_spring_security.entity.Role;
import com.spring.jwt_spring_security.entity.User;
import com.spring.jwt_spring_security.exception.BadRequestException;
import com.spring.jwt_spring_security.repository.mysql.RoleRepository;
import com.spring.jwt_spring_security.repository.mysql.UserRepository;
import com.spring.jwt_spring_security.service.UserService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  private final RoleRepository roleRepository;

  private final PasswordEncoder passwordEncoder;

  @Override
  public Optional<User> findByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  @Override
  public void saveUser(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    user.setRoles(new HashSet<>());
    userRepository.save(user);
  }

  @Override
  public void saveRole(Role role) {
    roleRepository.save(role);
  }

  @Override
  @Transactional
  public void addRoleToUser(String username, String roleName) {
    Optional<User> user = userRepository.findByUsername(username);
    Optional<Role> role = roleRepository.findByName(roleName);
    if (user.isPresent() && role.isPresent()) {
      user.get().getRoles().add(role.get());
    }
  }

  @Override
  @Transactional
  public void getRoleByUsername(String username) {
    List<Role> roles = roleRepository.getRole(username);
    List<RoleResponse> roleResponse = new ArrayList<>();
    roles.forEach(role -> roleResponse.add(
        new RoleResponse(role.getId(), role.getName(), role.getDescription())));
  }

  @Override
  public String test(int pram1) {
    if (pram1 == 2) {
      throw new BadRequestException("Lỗi rồi nhé");
    }

    return "hehe";

  }
}
