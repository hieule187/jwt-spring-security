package com.spring.jwt_spring_security.service.implement;

import com.spring.jwt_spring_security.config.base.BaseServiceImpl;
import com.spring.jwt_spring_security.entity.Product;
import com.spring.jwt_spring_security.repository.mysql.ProductRepository;
import com.spring.jwt_spring_security.service.ProductService;
import com.spring.jwt_spring_security.service.RabbitmqService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

  @Value("${application.rabbitmq.queue.create_product}")
  private String queueCreateProduct;

  @Value("${application.kafka.topic.create_product}")
  private String topicCreateProduct;

  private final ProductRepository repository;
  private final RabbitmqService rabbitmqService;
  private final KafkaTemplate<String, Object> kafkaTemplate;

  public ProductServiceImpl(
      ProductRepository repository,
      RabbitmqService rabbitmqService,
      KafkaTemplate<String, Object> kafkaTemplate
  ) {
    super(repository);
    this.repository = repository;
    this.rabbitmqService = rabbitmqService;
    this.kafkaTemplate = kafkaTemplate;
  }


  @Override
  public void rabbitmqCreate(Product product) {
    rabbitmqService.push(queueCreateProduct, product);
  }

  @Override
  public void kafkaCreate(Product product) {
    kafkaTemplate.send(topicCreateProduct, product);
  }

}
