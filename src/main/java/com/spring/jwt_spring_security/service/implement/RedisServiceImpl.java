package com.spring.jwt_spring_security.service.implement;

import com.spring.jwt_spring_security.model.Token;
import com.spring.jwt_spring_security.service.RedisService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisServiceImpl implements RedisService {

  @Value("${spring.redis.key.token-key:REDIS_USER_ID_TOKEN}")
  private String redisUserIdToken;

  private final RedisTemplate<Object, Object> redisTemplate;

  private final HashOperations<Object, Object, Token> hashOperations;

  public RedisServiceImpl(RedisTemplate<Object, Object> redisTemplate) {
    this.redisTemplate = redisTemplate;
    this.hashOperations = redisTemplate.opsForHash();
  }

  @Override
  public void saveToken(Token token) {
    hashOperations.put(redisUserIdToken, token.getUserId(), token);
  }

  @Override
  public Map<Object, Token> findAllToken() {
    return hashOperations.entries(redisUserIdToken);
  }

  @Override
  public Token findTokenByUserId(Long userId) {
    return hashOperations.get(redisUserIdToken, userId);
  }

  @Override
  public void updateToken(Token token) {
    saveToken(token);
  }

  @Override
  public void deleteToken(Long userId) {
    hashOperations.delete(redisUserIdToken, userId);
  }

}
