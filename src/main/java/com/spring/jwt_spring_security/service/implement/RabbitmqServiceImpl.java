package com.spring.jwt_spring_security.service.implement;

import com.spring.jwt_spring_security.service.RabbitmqService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitmqServiceImpl implements RabbitmqService {

  private final RabbitTemplate rabbitTemplate;
  private final RabbitTemplate rabbitTemplateSimple;

  public RabbitmqServiceImpl(RabbitTemplate rabbitTemplate, RabbitTemplate rabbitTemplateSimple) {
    this.rabbitTemplate = rabbitTemplate;
    this.rabbitTemplateSimple = rabbitTemplateSimple;
  }

  @Override
  public void push(String queue, String message) {
    log.info("(push)queue: {}, message: {}", queue, message);
    rabbitTemplateSimple.convertAndSend(queue, message);
  }

  @Override
  public void push(String queue, Object message) {
    log.info("(push)queue: {}, message: {}", queue, message);
    rabbitTemplate.convertAndSend(queue, message);
  }

  @Override
  public void push(String queue, List<Object> messages) {
    log.info("(push)queue: {}, messages: {}", queue, messages.size());
    for (Object message : messages) {
      rabbitTemplate.convertAndSend(queue, message);
    }
  }

}
