package com.spring.jwt_spring_security.controller;

import com.spring.jwt_spring_security.constant.RoleConstant;
import com.spring.jwt_spring_security.dto.ProductDTO;
import com.spring.jwt_spring_security.entity.Product;
import com.spring.jwt_spring_security.service.ProductService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/product")
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
public class ProductController {

  private final ProductService productService;

  @PostMapping("")
  @RolesAllowed({RoleConstant.ADMIN, RoleConstant.USER})
  public void create(@RequestBody @Valid ProductDTO request) {
    Product product = Product.of(
        request.getProductName(),
        request.getProductPrice(),
        request.getProductQuantity()
    );
    productService.kafkaCreate(product);
  }

  @PutMapping("/{id}")
  @RolesAllowed({RoleConstant.ADMIN, RoleConstant.USER})
  public Product update(
      @PathVariable @Valid String id,
      @RequestBody @Valid ProductDTO request
  ) {
    Product product = productService.findByIdThrowNotFound(id);
    product.setProductName(request.getProductName());
    product.setProductPrice(request.getProductPrice());
    product.setProductQuantity(request.getProductQuantity());
    return productService.update(product);
  }

}
