package com.spring.jwt_spring_security.controller;

import com.spring.jwt_spring_security.model.Token;
import com.spring.jwt_spring_security.service.RedisService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/token/public")
@RequiredArgsConstructor
public class TokenController {

  private final RedisService redisService;

  @PostMapping
  public void addToken(@RequestBody Token token) {
    redisService.saveToken(token);
  }

  @GetMapping
  public Map<Object, Token> getAllToken() {
    return redisService.findAllToken();
  }

  @GetMapping("/{userId}")
  public Token getTokenByUserId(@PathVariable Long userId) {
    return redisService.findTokenByUserId(userId);
  }

  @PutMapping
  public void updateToken(@RequestBody Token token) {
    Token existingToken = redisService.findTokenByUserId(token.getUserId());
    if (existingToken != null) {
      redisService.updateToken(token);
    }
  }

  @DeleteMapping("/{userId}")
  public void deleteToken(@PathVariable Long userId) {
    redisService.deleteToken(userId);
  }

}
