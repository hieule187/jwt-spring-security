package com.spring.jwt_spring_security.controller;

import com.spring.jwt_spring_security.config.authentication.JwtService;
import com.spring.jwt_spring_security.dto.request.SignInRequest;
import com.spring.jwt_spring_security.dto.response.SignInResponse;
import com.spring.jwt_spring_security.entity.Role;
import com.spring.jwt_spring_security.entity.User;
import com.spring.jwt_spring_security.exception.CustomException;
import com.spring.jwt_spring_security.repository.mysql.UserRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

  private final AuthenticationManager authenticationManager;
  private final JwtService jwtService;
  private final PasswordEncoder passwordEncoder;
  private final UserRepository userRepository;

  @PostMapping("/sign-in")
  public ResponseEntity<SignInResponse> signIn(
      @RequestBody @Validated SignInRequest signInRequest) {
    Optional<User> userCc = userRepository.findByUsername(signInRequest.getUsername());
    if (userCc.isEmpty() || !passwordEncoder.matches(signInRequest.getPassword(),
        userCc.get().getPassword())) {
      throw new CustomException(
          HttpServletResponse.SC_UNAUTHORIZED,
          "com.spring.jwt_spring_security.exception.UsernameOrPasswordIncorrectException"
      );
    }
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            signInRequest.getUsername(),
            signInRequest.getPassword())
    );
    User user = (User) authentication.getPrincipal();
    String jwtToken = jwtService.generateToken(user);
    String refreshToken = jwtService.generateRefreshToken(user);
    List<String> roles = user.getRoles().stream()
        .map(Role::getName)
        .distinct()
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK)
        .body(new SignInResponse(
            jwtToken, refreshToken, user.getId(), user.getUsername(), roles)
        );
  }

}
