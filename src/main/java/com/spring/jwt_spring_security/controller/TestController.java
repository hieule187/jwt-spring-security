package com.spring.jwt_spring_security.controller;

import com.spring.jwt_spring_security.constant.RoleConstant;
import com.spring.jwt_spring_security.dto.request.SignInRequest;
import com.spring.jwt_spring_security.exception.BadRequestException;
import com.spring.jwt_spring_security.exception.NotFoundException;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/test")
@SecurityRequirement(name = "bearerAuth")
public class TestController {

  @GetMapping("/get-1")
  @RolesAllowed({RoleConstant.ADMIN, RoleConstant.MANAGER})
  public String get1() {
    return SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
  }

  @GetMapping("/get-2")
  @RolesAllowed({RoleConstant.USER_PREMIUM})
  public String get2() {
    return SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
  }

  @GetMapping("/public/get-3")
  public String get3() {
    throw new NotFoundException("Not found!");
  }

  @GetMapping("/public/get-4")
  public String get4() {
    throw new BadRequestException("Error!");
  }

  @GetMapping("/public/get-5")
  public String get5(
      @RequestParam @Valid String testId,
      @RequestParam @Valid Integer testNumber
  ) {
    return "Success!";

  }

  @PostMapping("/public/get-6")
  public String get6(
      @RequestBody @Valid SignInRequest request
  ) {
    return "Success!";

  }

  @GetMapping("/public/get-7/{testId}")
  public String get7(@PathVariable Integer testId) {
    return "Success!";

  }

}
