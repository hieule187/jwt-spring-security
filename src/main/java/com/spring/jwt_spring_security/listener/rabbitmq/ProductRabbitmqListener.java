package com.spring.jwt_spring_security.listener.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.jwt_spring_security.entity.Product;
import com.spring.jwt_spring_security.service.ProductService;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProductRabbitmqListener {

  private final ProductService productService;

  @RabbitListener(queues = "${application.rabbitmq.queue.create_product}")
  public void rabbitmqCreateProduct(Message message) {
    if (Objects.isNull(message)) {
      return;
    }

    try {
      log.info("(rabbitmqCreateProduct)message: {}", message);
      Product product = new ObjectMapper().readValue(message.getBody(), Product.class);
      log.info("(rabbitmqCreateProduct)product: {}", product);
      productService.create(product);
    } catch (Exception exception) {
      log.error(
          "(rabbitmqCreateProduct)message: {}, exception: {}",
          message,
          ExceptionUtils.getStackTrace(exception)
      );
    }
  }

}
