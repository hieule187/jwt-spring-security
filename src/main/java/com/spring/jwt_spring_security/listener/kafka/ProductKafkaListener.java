package com.spring.jwt_spring_security.listener.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.jwt_spring_security.entity.Product;
import com.spring.jwt_spring_security.service.ProductService;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProductKafkaListener {

  private final ProductService productService;

  @KafkaListener(
      topics = "${application.kafka.topic.debezium.kafka_topic_product}",
      groupId = "consumer-product"
  )
  public void consumerProductListener(@Payload(required = false) String message) {
    if (Objects.isNull(message)) {
      return;
    }

    try {
      log.info("(consumerProductListener)message: {}", message);
//      CDC cdc = MapperUtil.getMapper().readValue(
//              message,
//              CDC.class
//          );
    } catch (Exception exception) {
      log.error(
          "(consumerProductListener)message: {}, exception: {}",
          message,
          ExceptionUtils.getStackTrace(exception)
      );
    }
  }

  @KafkaListener(
      topics = "${application.kafka.topic.create_product}",
      groupId = "create-product"
  )
  public void kafkaCreateProduct(@Payload(required = false) String message) {
    if (Objects.isNull(message)) {
      return;
    }

    try {
      log.info("(kafkaCreateProduct)message: {}", message);
      Product product = new ObjectMapper().readValue(message, Product.class);
      productService.create(product);
    } catch (Exception exception) {
      log.error(
          "(kafkaCreateProduct)message: {}, exception: {}",
          message,
          ExceptionUtils.getStackTrace(exception)
      );
    }
  }

}
